import { CircularProgress, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import TicketHeader from "./components/Header/TicketHeader";
import TicketItem from "./components/Ticket/TicketItem";
import useTicketHook from "./hooks/useTicketHook";
import InfiniteScroll from "react-infinite-scroll-component";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles(() => ({
    itemWrapper: {
        margin: "0px 18px",
        height: 620,
        width: "100%",
        overflow: "auto",
        position: "relative",
    },

    circularStyle: {
        position: "fixed",
        top: "25%",
        left: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        textAlign: "center",
    },
}));

const Tickets = (): JSX.Element => {
    const classes = useStyles();
    const {
        ticketList,
        error,
        hasMore,
        getTicketList,
        setFilterState,
        initializeTicketList,
        state,
    } = useTicketHook();
    return (
        <Grid container>
            <TicketHeader
                setFilterState={setFilterState}
                initializeTicketList={initializeTicketList}
                state={state}
            />

            <Grid id='scrollableDiv' className={classes.itemWrapper}>
                {error ? (
                    <Alert severity='error'>{error}</Alert>
                ) : (
                    <InfiniteScroll
                        dataLength={ticketList.length}
                        next={getTicketList}
                        hasMore={hasMore}
                        loader={
                            <CircularProgress
                                className={classes.circularStyle}
                            />
                        }
                        scrollableTarget='scrollableDiv'
                    >
                        {ticketList.map((item, index) => (
                            <TicketItem item={item} key={index} />
                        ))}
                    </InfiniteScroll>
                )}
            </Grid>
        </Grid>
    );
};

export default Tickets;
