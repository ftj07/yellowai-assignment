export interface Issue {
    title: string;
    labels: Label[];
    user: User;
    state: string;
}


export interface Label {
    id: number;
    node_id: string;
    url: string;
    name: string;
    color: string;
    default: boolean;
    description: string;
}

export interface User {
    login: string;
    avatar_url: string;
}