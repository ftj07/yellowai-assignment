import { Issue } from "./Issue.interface";

export interface TicketHookReturnType {
    ticketList: Issue[];
    error: string;
    hasMore: boolean;
    getTicketList: () => Promise<void>;
    setFilterState: React.Dispatch<React.SetStateAction<string>>;
    initializeTicketList: () => void;
    state: string
}