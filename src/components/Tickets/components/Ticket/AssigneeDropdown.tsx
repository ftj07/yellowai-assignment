import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import { ClassNameMap } from "../../types/classNameMap.type";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
interface Props {
    classes: ClassNameMap;
    assignee: string;
}

const AssigneeDropdown = ({ classes, assignee }: Props): JSX.Element => {
    return (
        <FormControl variant='outlined' className={classes.mr16}>
            <Grid className={classes.selectWrapper}>
                <PersonOutlineIcon />
                <Select
                    classes={{
                        root: classes.selectRoot,
                    }}
                    value={assignee}
                >
                    <MenuItem value={assignee}>
                        <span className={classes.textTransformation}>
                            {assignee}
                        </span>
                    </MenuItem>
                </Select>
            </Grid>
        </FormControl>
    );
};

export default AssigneeDropdown;
