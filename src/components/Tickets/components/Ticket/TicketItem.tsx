import Avatar from "@material-ui/core/Avatar";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import { Issue } from "../../interfaces/Issue.interface";
import AssigneeDropdown from "./AssigneeDropdown";
import LabelItems from "./LabelItems";
import PriorityDropdown from "./PriorityDropdown";
import StateDropdown from "./StateDropdown";
const useStyles = makeStyles(() => ({
    wrapper: {
        marginBottom: 16,
        padding: "24px 0",
        borderRadius: 4,
        border: "1px solid rgba(63, 63, 68, 0.15)",

        "& fieldset": {
            border: "none",
        },

        "& .MuiSelect-select: focus": {
            borderColor: "none",
        },
        "& .MuiSvgIcon-root": {
            fontSize: "20px",
        },
    },
    mr16: {
        marginRight: 16,
    },

    mx8: {
        margin: "0px 8px",
    },
    mtb8: {
        margin: "8px 0",
    },
    title: {
        fontWeight: 500,
        fontSize: 16,
        letterSpacing: " 0.15px",
        color: "#384248",
    },
    selectWrapper: {
        display: "flex",
        alignItems: "center",
        border: "1px solid #C4C4C4",
        borderRadius: 4,
        padding: "0px 18px",
    },
    selectRoot: {
        fontSize: 14,
    },
    textTransformation: {
        textTransform: "capitalize",
    },
    subtitle: {
        fontSize: 14,
        letterSpacing: 0.15,
        color: "#505862",
    },
    maximizeIcon: {
        marginTop: 10,
        color: "gold",
    },
}));

interface Props {
    item: Issue;
}

const TicketItem = ({ item }: Props): JSX.Element => {
    const classes = useStyles();

    return (
        <Grid container alignItems='center' className={classes.wrapper}>
            <Grid item xs={7}>
                <Grid container>
                    <Grid item>
                        <Checkbox
                            inputProps={{
                                "aria-label": "uncontrolled-checkbox",
                            }}
                            className={classes.mx8}
                        />
                    </Grid>
                    <Grid item className={classes.mr16}>
                        <Avatar
                            alt={item.user.login}
                            src={item.user.avatar_url}
                        />
                    </Grid>
                    <Grid item>
                        <Grid container direction='column'>
                            <Grid item>
                                <LabelItems labels={item.labels} />
                            </Grid>
                            <Grid
                                item
                                className={clsx(classes.mtb8, classes.title)}
                            >
                                {item.title}
                            </Grid>
                            <Grid className={classes.subtitle}>
                                <span className={classes.mr16}>
                                    By Akshat Dhabi
                                </span>
                                <span className={classes.mr16}>
                                    <span className={classes.mr16}>
                                        &#x2022;
                                    </span>
                                    Created 8 hours ago
                                </span>
                                <span>
                                    <span className={classes.mr16}>
                                        &#x2022;
                                    </span>
                                    Overdue by 8 hours ago
                                </span>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={5}>
                <PriorityDropdown classes={classes} />
                <StateDropdown state={item.state} classes={classes} />
                <AssigneeDropdown
                    assignee={item.user.login}
                    classes={classes}
                />
            </Grid>
        </Grid>
    );
};

export default TicketItem;
