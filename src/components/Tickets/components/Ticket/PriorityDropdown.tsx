import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import TuneIcon from "@material-ui/icons/Tune";
import { useState } from "react";
import { ClassNameMap } from "../../types/classNameMap.type";
import MaximizeIcon from "@material-ui/icons/Maximize";

interface Props {
    classes: ClassNameMap;
}

const PriorityDropdown = ({ classes }: Props): JSX.Element => {
    const [priority, setPriority] = useState("high");

    const handlePriorityChange = (event: any) => {
        setPriority(event.target.value);
    };
    return (
        <FormControl variant='outlined' className={classes.mr16}>
            <Grid className={classes.selectWrapper}>
                <MaximizeIcon className={classes.maximizeIcon} />
                <Select
                    classes={{
                        root: classes.selectRoot,
                    }}
                    value={priority}
                    onChange={handlePriorityChange}
                >
                    <MenuItem value='high'>High</MenuItem>
                    <MenuItem value='medium'>Medium</MenuItem>
                    <MenuItem value='low'>Low</MenuItem>
                </Select>
            </Grid>
        </FormControl>
    );
};

export default PriorityDropdown;
