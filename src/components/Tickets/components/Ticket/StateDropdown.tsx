import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import TimelineIcon from "@material-ui/icons/Timeline";
import State from "../../enum/state";
import { ClassNameMap } from "../../types/classNameMap.type";

interface Props {
    classes: ClassNameMap;
    state: string;
}

const StateDropdown = ({ classes, state }: Props): JSX.Element => {
    // const [, setPriority] = useState();

    // const handlePriorityChange = (event: any) => {
    //     setPriority(event.target.value);
    // };
    return (
        <FormControl variant='outlined' className={classes.mr16}>
            <Grid className={classes.selectWrapper}>
                <TimelineIcon />
                <Select
                    classes={{
                        root: classes.selectRoot,
                    }}
                    value={state}
                >
                    <MenuItem value={State.OPEN}>
                        <span className={classes.textTransformation}>
                            {State.OPEN}
                        </span>
                    </MenuItem>
                    <MenuItem value={State.CLOSED}>
                        <span className={classes.textTransformation}>
                            {State.CLOSED}
                        </span>
                    </MenuItem>
                    <MenuItem value={State.ALL}>
                        <span className={classes.textTransformation}>
                            {State.ALL}
                        </span>
                    </MenuItem>
                </Select>
            </Grid>
        </FormControl>
    );
};

export default StateDropdown;
