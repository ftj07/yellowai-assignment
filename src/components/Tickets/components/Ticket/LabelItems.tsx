import { Label } from "../../interfaces/Issue.interface";
import LabelItem from "./LabelItem";

const LabelItems = ({ labels }: { labels: Label[] }): JSX.Element => {
    return (
        <>
            {labels.map((label, index) => (
                <LabelItem label={label} key={index} />
            ))}
        </>
    );
};

export default LabelItems;
