import Chip from "@material-ui/core/Chip";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Label } from "../../interfaces/Issue.interface";

const useStyles = makeStyles(() => ({
    mr8: {
        marginRight: 8,
    },
}));

const LabelItem = ({ label }: { label: Label }): JSX.Element => {
    const classes = useStyles();
    return (
        <Chip
            className={classes.mr8}
            variant='outlined'
            size='small'
            style={{
                border: `1px solid #${label.color}`,
                color: ` #${label.color}`,
                background: `rgba(#${label.color}, 0.10)`,
            }}
            label={label.name}
        />
    );
};

export default LabelItem;
