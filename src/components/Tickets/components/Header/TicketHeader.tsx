import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SwapVertIcon from "@material-ui/icons/SwapVert";
import TuneIcon from "@material-ui/icons/Tune";
import React, { useState } from "react";
import State from "../../enum/state";
import clsx from "clsx";
interface Props {
    setFilterState: React.Dispatch<React.SetStateAction<string>>;
    initializeTicketList: () => void;
    state: string;
}

const useStyles = makeStyles(() => ({
    wrapper: {
        padding: "29px 24px",
    },
    mr16: {
        marginRight: 16,
    },

    titleWrapper: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    titleColor: {
        color: "#384248",
    },
    titleIconColor: {
        color: "#707E8A",
    },
    btn: {
        textTransform: "capitalize",
        background: "#F8F8F8",
        fontSize: 14,
        color: "#384248",
    },
}));

const TicketHeader = ({
    setFilterState,
    initializeTicketList,
    state,
}: Props): JSX.Element => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const setStateValue = (e: React.MouseEvent<HTMLElement>) => {
        const dataSetValue = e.currentTarget.dataset.value as string;
        if (state !== dataSetValue) {
            initializeTicketList();
            setFilterState(dataSetValue);
        }
        setAnchorEl(null);
    };
    return (
        <Grid
            container
            justifyContent='space-between'
            alignItems='center'
            className={classes.wrapper}
        >
            <Grid item className={classes.titleWrapper}>
                <Typography
                    className={clsx(classes.titleColor, classes.mr16)}
                    variant='subtitle1'
                    component='span'
                >
                    All tickets
                </Typography>
                <ArrowDropDownIcon className={classes.titleIconColor} />
            </Grid>
            <Grid item>
                <Button
                    variant='outlined'
                    color='default'
                    startIcon={<SwapVertIcon />}
                    className={clsx(classes.mr16, classes.btn)}
                >
                    Sort by Due Date
                </Button>
                <Button
                    variant='outlined'
                    color='default'
                    aria-owns={anchorEl ? "menu-appbar" : undefined}
                    aria-haspopup='true'
                    onClick={handleMenu}
                    className={classes.btn}
                    startIcon={<TuneIcon />}
                >
                    Filters
                </Button>
                <Menu
                    id='menu-appbar'
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem onClick={setStateValue} data-value={State.OPEN}>
                        Open
                    </MenuItem>
                    <MenuItem onClick={setStateValue} data-value={State.CLOSED}>
                        Closed
                    </MenuItem>
                    <MenuItem onClick={setStateValue} data-value={State.ALL}>
                        All
                    </MenuItem>
                </Menu>
            </Grid>
        </Grid>
    );
};

export default TicketHeader;
