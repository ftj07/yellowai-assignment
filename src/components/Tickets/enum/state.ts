enum State {
    OPEN = 'open',
    CLOSED = 'closed',
    ALL = 'all'
}

export default State;