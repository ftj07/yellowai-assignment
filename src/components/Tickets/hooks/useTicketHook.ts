import { useEffect, useState } from "react";
import axios from 'axios';
import { Issue } from "../interfaces/Issue.interface";
import { TicketHookReturnType } from "../interfaces/TicketHook.interface";



const useTicketHook = (): TicketHookReturnType => {
    const [ticketList, setTicketList] = useState([] as Issue[]);
    const [error, setError] = useState('');
    const [hasMore, setHasMore] = useState(true);
    const [page, setPage] = useState<number>(1);
    const [state, setFilterState] = useState<string>('all');


    const fetchTicketList = async (): Promise<Issue[] | undefined> => {
        try {
            const result = await axios.get(`https://api.github.com/repos/yugabyte/yugabyte-db/issues?state=${state}&per_page=10&page=${page}`);
            return result.data;
        } catch {
            setError("Please try again after while")
        }
    }

    const getTicketList = async (): Promise<void> => {
        try {
            const result = await fetchTicketList();
            if (result) {
                const newList = [...ticketList, ...result];
                setTicketList(newList);
                const newPageValue = page + 1;
                setPage(newPageValue);
            } else {
                setHasMore(false);
            }
        } catch {
            setError("Please try again after while")
        }
    }

    const initializeTicketList = () => {
        setTicketList([]);
        setError('');
        setHasMore(true);
        setPage(1);

    }


    useEffect(() => {
        getTicketList();
    }, [state]);



    return { ticketList, error, hasMore, getTicketList, setFilterState, initializeTicketList, state }
}

export default useTicketHook;