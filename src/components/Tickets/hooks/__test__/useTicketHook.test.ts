import { renderHook } from '@testing-library/react-hooks';
import useTicketHook from '../useTicketHook';
describe('Testing Ticket Hook', () => {
    let result: any;
    beforeEach(() => {
        result = renderHook(() => useTicketHook());
        result = result.result.current;
    });

    it('should ticketlist empty array', () => {
        expect(result.ticketList).toEqual([]);
    });

})