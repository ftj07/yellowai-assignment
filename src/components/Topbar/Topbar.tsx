import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import { useState } from "react";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import Avatar from "@material-ui/core/Avatar";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
const useStyle = makeStyles(() => ({
    muibuttonRoot: {
        fontSize: 14,
        fontWeight: "normal",
        textTransform: "capitalize",
        cursor: "pointer",
    },
    wrapper: {
        padding: "0 26px",
        boxShadow: " 0px 1px 0px #D1D1D1",
    },
    mr32: {
        marginRight: 32,
    },
}));

const Topbar = (): JSX.Element => {
    const classes = useStyle();
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Grid container alignItems='center' className={classes.wrapper}>
            <Grid item xs={6}>
                <Grid container>
                    <Button
                        className={classes.mr32}
                        classes={{
                            root: classes.muibuttonRoot,
                        }}
                    >
                        Overview
                    </Button>
                    <Button
                        className={classes.mr32}
                        classes={{
                            root: classes.muibuttonRoot,
                        }}
                    >
                        My tickets
                    </Button>
                    <Button
                        className={classes.mr32}
                        classes={{
                            root: classes.muibuttonRoot,
                        }}
                    >
                        Archive
                    </Button>
                    <Button
                        className={classes.mr32}
                        classes={{
                            root: classes.muibuttonRoot,
                        }}
                    >
                        Analytics
                    </Button>
                    <Button
                        className={classes.mr32}
                        classes={{
                            root: classes.muibuttonRoot,
                        }}
                    >
                        Reports
                    </Button>
                    <Button
                        className={classes.mr32}
                        classes={{
                            root: classes.muibuttonRoot,
                        }}
                    >
                        Settings
                    </Button>
                </Grid>
            </Grid>
            <Grid item xs={6}>
                <Grid container justifyContent='flex-end' alignItems='center'>
                    <IconButton
                        aria-label='Open notification menu'
                        color='inherit'
                    >
                        <NotificationsNoneIcon />
                    </IconButton>
                    <IconButton
                        aria-label='Open notification menu'
                        color='inherit'
                    >
                        <HelpOutlineIcon />
                    </IconButton>
                    <Grid>
                        <IconButton
                            aria-label='account of current user'
                            aria-controls='menu-appbar'
                            aria-haspopup='true'
                            onClick={handleMenu}
                            color='inherit'
                        >
                            <Avatar
                                alt='Remy Sharp'
                                src='/static/images/avatar/1.jpg'
                            />
                            <ArrowDropDownIcon />
                        </IconButton>

                        <Menu
                            id='menu-appbar'
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            open={open}
                            onClose={handleClose}
                        >
                            <MenuItem>Profile</MenuItem>
                            <MenuItem>My account</MenuItem>
                        </Menu>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Topbar;
