import { TypographyOptions } from '@material-ui/core/styles/createTypography';

const typography: TypographyOptions = {
  fontFamily: 'Roboto',
  htmlFontSize: 14,
  body1: {
    lineHeight: '140%',
    fontSize: '1rem',
    letterSpacing: '1px',
  },
  body2: {
    fontSize: '0.93rem',
    lineHeight: '140%',
    letterSpacing: '1px',
  },

  h1: {
    fontWeight: 500,
    fontSize: '3.43rem',
    lineHeight: '120%',
    letterSpacing: '-1px',
  },
  h2: {
    fontWeight: 500,
    fontSize: '2.29rem',
    lineHeight: '120%',
    letterSpacing: '-0.5px',
  },
  h3: {
    fontWeight: 500,
    fontSize: '1.714rem',
    lineHeight: '28px',
    letterSpacing: '-0.5px',
  },
  h4: {
    fontWeight: 500,
    fontSize: '1.29rem',
    lineHeight: '21px',
    textTransform: 'capitalize',
  },
  subtitle1: {
    fontSize: '2.3rem',
    fontWeight: 600
  },
  subtitle2: {
    fontSize: '0.86rem',
    lineHeight: '140%',
    fontWeight: 'normal',
  },
  caption: {
    fontSize: '0.79rem',
    lineHeight: '140%',
    letterSpacing: '0.5px',
  },
  overline: {
    fontWeight: 'bold',
    fontSize: '0.86rem',
    lineHeight: '140%',
    letterSpacing: '0.75px',
    textTransform: 'uppercase',
  },
  button: {
    fontSize: '0.86rem',
    fontWeight: 'bold',
    letterSpacing: '1.25px',
    textTransform: 'uppercase',
  },
};

export default typography;
