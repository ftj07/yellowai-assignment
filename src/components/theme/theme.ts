import { createTheme } from "@material-ui/core";
import typography from './typography'

const theme = createTheme({
    typography: typography,

});

export default theme;