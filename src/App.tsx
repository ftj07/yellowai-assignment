import { Grid } from "@material-ui/core";
import Tickets from "./components/Tickets/Tickets";
import Topbar from "./components/Topbar/Topbar";

const App = (): JSX.Element => {
    return (
        <Grid container direction='column'>
            <Topbar />
            <Tickets />
        </Grid>
    );
};

export default App;
